#!/usr/bin/perl -wT

# Copyright 1999,2000 James Treacy.
# Permission is granted to use this under the GNU GPL.

# used to select a mirror from the pulldown menu on the www.d.o front page

require 5.001;
use strict;
use CGI;

MAIN:
{
  my ($input,   # The CGI data
      $site);

  $ENV{PATH} = "/bin:/usr/bin";
  # Read in all the variables set by the form
  $input = new CGI;

  # If you want, just print out a list of all of the variables.
  # print $input->dump;
  # exit;

  # print $input->header('text/html'); -- not allowed according to CGI(3pm)
  $site = $input->param('site');
  if ($input->param('page') && ($site =~ /^\w\w$/)) {
    print $input->redirect("http://www.".$site.".debian.org".$input->param('page'))
  } else {
    print $input->header( -status => 400 );
    print $input->start_html("Malformed parameters");
    print $input->h1("Malformed parameters");
    print $input->p("Sorry, couldn't redirect because one or more parameters were missing and/or malformed");
    print $input->end_html;
  }
}
